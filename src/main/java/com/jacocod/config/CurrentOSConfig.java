package com.jacocod.config;

import com.jacocod.common.Interface.ISystem;
import com.jacocod.common.contains.System;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Locale;

@Configuration
public class CurrentOSConfig {

    private String osName() {
        return java.lang.System.getProperty("os.name").split(" ")[0].toUpperCase(Locale.ROOT);
    }

    @Bean
    public ISystem iSystem() {
        return System.valueOf(osName());
    }
}
