package com.jacocod;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class JaApplication {

    public static void main(String[] args) {
        SpringApplication.run(JaApplication.class, args);
    }

}
