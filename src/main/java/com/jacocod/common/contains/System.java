package com.jacocod.common.contains;

import com.jacocod.common.Interface.ISystem;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;


@Configuration
public enum System implements ISystem {
    WINDOWS {
        @Value("${project.workspace.windows}")
        private String windowsBaseWorkSpace;

        @Override
        public String baseWorkSpace() {
            return windowsBaseWorkSpace;
        }
    },
    LINUX {
        @Value("${project.workspace.linux}")
        private String linuxBaseWorkSpace;

        @Override
        public String baseWorkSpace() {
            return linuxBaseWorkSpace;
        }
    },
    MAC {
        @Value("${project.workspace.mac}")
        private String macBaseWorkSpace;

        @Override
        public String baseWorkSpace() {
            return macBaseWorkSpace;
        }
    };
}
