package com.jacocod.common.contains;

public enum ApplicationError {

    NOT_SUPPORT_SYSTEM("不支持的系统",500,"NOT_SUPPORT_SYSTEM");;
    private String message;
    private Integer code;
    private String status;



    ApplicationError(String message, Integer code, String status) {
        this.message = message;
        this.code = code;
        this.status = status;
    }
}
