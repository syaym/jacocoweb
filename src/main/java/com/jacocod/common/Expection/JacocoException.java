package com.jacocod.common.Expection;


import lombok.AllArgsConstructor;

@AllArgsConstructor
public class JacocoException extends RuntimeException {
    private String name;
    private Integer code;
    private String status;

}
