package com.jacocod.common.Interface;

import org.eclipse.jgit.api.Git;

public interface IRepository {
    /**
     * 克隆代码
     */
    void cloneCode();

    /**
     * 切换分支
     */
    void checkout();

    /**
     * 拉取
     */
    void pull();

    /**
     * 初始化
     */
    void init();

    /**
     * 拉取远程分支
     * @param branch
     */
    void fetch(String branch);
}
