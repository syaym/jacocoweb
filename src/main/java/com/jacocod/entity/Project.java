package com.jacocod.entity;

import lombok.Data;

@Data
public class Project extends BaseEntity{
    private String name;
}
