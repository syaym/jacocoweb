package com.jacocod.entity;

import lombok.Data;

@Data
public class WorkSpaceEntity {
    private String basePath;
    private String dirName;
}
