package com.jacocod.entity;

import lombok.Data;

import java.util.Date;

@Data
public class BaseEntity {
    private String id;
    private Date updateTime;
    private Date createTime;
}
