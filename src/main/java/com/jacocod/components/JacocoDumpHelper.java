package com.jacocod.components;

import org.jacoco.core.data.ExecutionDataWriter;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

import org.jacoco.core.runtime.RemoteControlReader;
import org.jacoco.core.runtime.RemoteControlWriter;


@Component
public class JacocoDumpHelper {

    @Resource
    private com.jacocod.common.Interface.ISystem iSystem;


    public void dumpExecData(String execFilePath, String address, Integer port) throws Exception {
        final FileOutputStream localFile = new FileOutputStream(execFilePath);
        final ExecutionDataWriter localWriter = new ExecutionDataWriter(
                localFile);
        final Socket socket = new Socket(InetAddress.getByName(address), port);
        final RemoteControlWriter writer = new RemoteControlWriter(socket.getOutputStream());
        final RemoteControlReader reader = new RemoteControlReader(socket.getInputStream());
        reader.setSessionInfoVisitor(localWriter);
        reader.setExecutionDataVisitor(localWriter);
        writer.visitDumpCommand(true, false);
        if (!reader.read()) {
            throw new IOException("Socket closed unexpectedly.");
        }
        socket.close();
        localFile.close();
    }
}


