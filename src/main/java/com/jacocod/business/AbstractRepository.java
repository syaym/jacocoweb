package com.jacocod.business;

import com.jacocod.common.Interface.IRepository;
import com.jacocod.entity.Person;

abstract public class AbstractRepository<T> implements IRepository {

    protected String workSpace;

    public AbstractRepository(String workSpace, Person person) {
        this.workSpace = workSpace;
        this.person = person;
        this.setRepositoryActionObject();
    }

    protected Person person;

    /**
     * 仓库操作对象
     */
    protected T repositoryActionObject;

    abstract void setRepositoryActionObject();
}
