package com.jacocod.business;

import com.jacocod.common.Expection.JacocoException;
import com.jacocod.entity.Person;
import org.eclipse.jgit.api.CloneCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;

import java.io.File;
import java.io.IOException;

public class GitRepository extends AbstractRepository<Git>{


    public GitRepository(String workSpace, Person person) {
        super(workSpace, person);
    }

    @Override
    public void cloneCode() {
        CloneCommand cloneCommand = Git.cloneRepository();
        try {
            cloneCommand.setCloneAllBranches(true).setDirectory(new File(this.workSpace)).call();
        } catch (GitAPIException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void checkout() {

    }

    @Override
    public void pull() {

    }

    @Override
    public void init() {

    }

    @Override
    public void fetch(String branch) {

    }

    @Override
    void setRepositoryActionObject() {
        Repository build;
        try {
            build = new FileRepositoryBuilder().setGitDir(new File(this.workSpace)).build();
            this.repositoryActionObject = new Git(build);
        } catch (IOException e) {
            throw new RuntimeException();
        }
    }
}
